from fastapi import FastAPI
from littlenv import littlenv
from app.api import urls

littlenv.load()

app = FastAPI()


app.include_router(
    urls.urls
)
