from sqlalchemy.ext.declarative import declarative_base
from littlenv import littlenv
from config.settings.db import conexion

littlenv.load()
Base = declarative_base()

conexion()
