from typing import Optional
from fastapi import APIRouter, responses
from starlette.responses import JSONResponse
from starlette import status

router = APIRouter()


@router.get("/items")
async def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


# @router.get("/items/{item_id}")
# async def read_item(item_id: int, q: Optional[str] = None):
#     x = None
#     return JSONResponse(status_code=status.HTTP_200_OK,
#                         content=x)
