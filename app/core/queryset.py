from app.core.models import User
from app.core.models import Profile
from app.core.models import UserProfile

from config.base import conexion


class GenericQuery:

    def __init__(self, entity):
        self.entity = entity

    def create(self, **kwarg):
        try:
            _instance = self.entity(**kwarg)
            conexion.add(_instance)
            conexion.commit()
            return _instance
        except Exception:
            conexion.rollback()
        finally:
            conexion.close()

    def get(self, **kwarg):
        _instance = conexion(self.entity).get(**kwarg)
        conexion.commit()
        return _instance

    def filter(self, **kwarg):
        _instance = conexion(self.entity).filter(**kwarg)
        conexion.commit()
        return _instance

    def updated(self, **kwarg):
        pass

    def delete(self, **kwarg):
        pass
